# BTVDD_CVAE

Conditional Variational autoencoder example

Built on Keras in Kaggle

update path to the data folder

Sample output plots: 
 the training loss
 the Z space of the training examples
 new samples drawn from a 10x10 map of Z space
 
Important - if run on CPU and not GPU will take a significant time to train